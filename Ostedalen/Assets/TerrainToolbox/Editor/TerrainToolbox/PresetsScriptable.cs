﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PresetsScriptable : ScriptableObject 
{
	public List<string> presetsNames = new List<string>();
}